import re
from pathlib import Path
import numpy as np

import pandas as pd

def data_files(folder):
    """Iterable with paths of every txt file in folder"""
    return (x for x in sorted(Path(folder).glob('*.txt')) if x.is_file())


class File_processor(object):

    def __init__(self):
        """File_processor is a helper object that can open Autoscala TXT
           datafiles, process them and return all their data and metadata
           as values of a dictionary.
           
           fp = File_processor()
           d = fp.process_file(filepath)
           
           d
           
           {'M(3000)F2': ('02.84', None),
             'MUF(3000)F2': ('20.7', 'MHz'),
             'day': '01',
             'foF1': (None, None),
             'foF2': ('07.3', 'MHz'),
             'ftEs': ('03.2', 'MHz'),
             'fxI': ('07.7', 'MHz'),
             "h'Es": ('099', 'km'),
             'hour': '06',
             'minute': '40',
             'month': '01',
             'station': 'Tucumán (Lat: -26.9, Lon: 294.6)',
             'timezone': '(UT)',
             'version': '2007',
             'year': '2014'}
        """

        self.pattern_hdr1 = re.compile(r'(?P<station>.*) - ' +
                          r'DATE: (?P<year>\d{4}) (?P<month>\d{2}) '+
                          r'(?P<day>\d{2}) - ' +
                          r'TIME (?P<timezone>\(UT\)): ' + 
                          r'(?P<hour>\d+):(?P<minute>\d+)')
        self.pattern_hdr2 = re.compile(r'Autoscala (?P<version>.+)')
        self.pattern_field = re.compile(r'(?P<param>\S+)\s+(NO|' +
                                         r'(?P<value>\d+(.\d+)?))(\s+' +
                                         r'(?P<unit>\w+))?')

    def process_file(self,filepath):
        """Process one Autoscala txt file
           returns a dictionary with all data and metadata fields or
           None if the file header isn't recognized."""
        d = {}
        with Path(filepath).open() as f:
            cur = iter(f)
            try:
                d.update(self.pattern_hdr1.match(next(cur)).groupdict())
                d.update(self.pattern_hdr2.match(next(cur)).groupdict())
            except: # Header must be what we expect, otherwise return empty
                return None
            try:
                while True:
                    match = self.pattern_field.match(next(cur))
                    if match:
                        field = match.groupdict()
                        if None != field['value']:
                            d[field['param']] = (field['value'], field['unit'])
                        else:
                            d[field['param']] = None
                    else:
                        # Stop at the first line that
                        # doesn't appear to be a field
                        break
            except StopIteration:
                pass # Early end of file, we still use the results
        return d


DATA_FIELDS = ['foF2', 'M(3000)F2']
METADATA_FIELDS = ['year','month','day','hour','minute']
# minute/60 is added to hour (there isn't a minute field in the table)
TABLE_HDR = METADATA_FIELDS[:-1] + DATA_FIELDS

def usable_entry(x):
    """Checks that a field dictionary has the minimum data and metadata
       for inclusion in the table"""
    return x and all(y in x and x[y] != None 
                     for y in METADATA_FIELDS + DATA_FIELDS )
def entry_to_row(x):
    rw=[]

    rw.append(float(x['year']))
    rw.append(float(x['month']))
    rw.append(float(x['day']))
    rw.append(float(x['hour'])+float(x['minute'])/60)

    for field in DATA_FIELDS:    
        rw.append(float(x[field][0]))
    return rw


if __name__=='__main__':

    DATADIR = 'ais_txts'
    TABLE_FILE = 'datos_ais.csv'
    
    processor = File_processor()


    table = pd.DataFrame([entry_to_row(y) for y in
                    (processor.process_file(x) for x in data_files(DATADIR)) 
                    if usable_entry(y)], columns = TABLE_HDR)
    table.to_csv(TABLE_FILE)
    
    
        