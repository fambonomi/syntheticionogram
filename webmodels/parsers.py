# -*- coding: utf-8 -*-
"""
    Parsers para las versiones webs de los modelos IRI y NeQuick2
"""
from html.parser import HTMLParser
import numpy as np

class NeQuick2ParserException(Exception):
     pass
class IRIParserException(Exception):
    pass

class ModelParser(HTMLParser):
    """Clase Base para derivar parsers de las versiones web de modelos."""

    def reset(self):
        """ Inicialización de atributos """
        HTMLParser.reset(self)
        self.current_tag = ""
        self.metadata = ""
        self.data = ""

    def handle_starttag(self, tag, args):
        """ Asigna la etiqueta actual al atributo current_tag """
        self.current_tag = tag

    def handle_data(self, data):
        """Define el comportamiento cada etiqueta. Debe discriminar y acumular
            los datos y metadatos según corresponda"""
        raise NotImplementedError

    def get_model_result(self):
        """ Debe retornar un diccionario que contenga las claves:
            metadata: Información extra que devuelve la salida de los modelos
            array: numpy.array con los resultados del modelos
            labels: lista de etiquetas de cada columna del array
            units: unidades de las columnas del array"""
        raise NotImplementedError


class IRIParser(ModelParser):
    """ Parser de la versión web del modelo IRI """

    def handle_data(self, data):
        """Discrimina entre metadatos y datos y los acumula o asigna a los atributos
            metadata y data"""
        if self.current_tag in ["br", "i", "b", "A"]:
            self.metadata += data + "\n"
        elif self.current_tag == "pre":
            self.data = data

    def get_model_result(self):
        """Retorna un diccionario con las siguientes claves:
            metadata: Información extra que devuelve la salida de los modelos
            array: numpy.array con los resultados del modelos
            labels: lista de etiquetas de cada columna del array
            units: unidades de las columnas del array
            lines = self.data.split("\n")
            self.metadata = "\n".join(lines[:12])"""

        lines = self.data.split("\n")
        labels = []
        i = 1
        while len(lines[i]) > 1:
            labels.append(lines[i])
            i += 1
        return dict(metadata=self.metadata,
                    array=np.array([np.array(x.split())
                                    for x in lines[i + 2:-1]]),
                    labels=labels)


class NeQuick2Parser(ModelParser):
    """ Parser de la versión web del modelo NeQuick2 """

    def handle_data(self, data):
        """Discrimina entre metadatos y datos y los acumula o asigna a los atributos
            metadata y data"""

        if self.current_tag == "pre":
            self.data = data

    def get_model_result(self):
        """Retorna un diccionario con las siguientes claves:
            metadata: Información extra que devuelve la salida de los modelos
            array: numpy.array con los resultados del modelos
            labels: lista de etiquetas de cada columna del array
            units: unidades de las columnas del array"""
        lines = self.data.split("\n")
        i = 0
        while lines[i].find("el.density") < 0:
            i += 1
        units_line = i + 1
        self.metadata = "\n".join(lines[:units_line])

        labels = [lines[units_line - 1].split()]
        units = [lines[units_line].split()]

        i = units_line
        while lines[i].find("Electron content") < 0:
            i += 1
            if i == len(lines):
                raise NeQuick2ParserException
        return dict(metadata=self.metadata,
                    array=np.array([np.array(x.split()).astype(float)
                                    for x in lines[units_line +1 :i-1]]),
                    labels=labels, units=units)
