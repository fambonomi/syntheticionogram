# -*- coding: utf-8 -*-
"""
    Presets de parámetros para modelos webs
"""
from collections import OrderedDict

IRI2016_DEFAULT_INPUT = OrderedDict([('model', 'iri_2016'),
                                     ('year', '2014'),
                                     ('month', '01'),
                                     ('day', '13'),  # Day of month
                                     ('time_flag', '0'),  # 0 UT 1 LT
                                     ('hour', '4.'),
                                     ('geo_flag', '0.'),
                                     ('latitude', '-26.9'),
                                     ('longitude', '294.6'),
                                     ('height', '100.'),
                                     ('profile', '1'),  # 1: Height Profile
                                     ('start', '80.'),
                                     ('stop', '1000.'),
                                     ('step', '10.'),
                                     ('sun_n', ''),
                                     ('ion_n', ''),
                                     ('radio_f', ''),
                                     ('radio_f81', ''),
                                     ('htec_max', ''),
                                     # 1.IRI01-corr 0.Nequick2
                                     ('ne_top', '0.'),
                                     ('imap', '1.'),  # 1. CCIR 0. URSI
                                     # Storm model 0. On 1. Off
                                     ('ffof2', '1.'),
                                     ('hhmf2', '0.'),  # F-peak h. 0.AMTB2013
                                                       # 1.SHU-2015 2. BSE-1979
                                     # BottomsideThickness 0.B 1.G 2.ABT2009
                                     ('ib0', '2.'),
                                     # F1 occurrence ScotoL S, I
                                     ('probab', '0.'),
                                     ('fauroralb', '1.'),
                                     ('ffoE', '1.'),
                                     ('dreg', '0.'),
                                     ('tset', '0.'),
                                     ('icomp', '0.'),
                                     ('nmf2', '0.'),
                                     ('hmf2', '0.'),
                                     ('user_nme', '0.'), #user fof2 or nmf2
                                     ('user_hme', '0.'), #user m3000 or hmf2
                                     ('user_B0', '0.'),
                                     ('format', '0'),
                                     ('vars', ['6', '17']),
                                     ('linestyle', 'solid'),
                                     ('charsize', ''),
                                     ('symbol', '2'),
                                     ('symsize', ''),
                                     ('yscale', 'Linear'),
                                     ('xscale', 'Linear'),
                                     ('imagex', '640'),
                                     ('imagey', '480')])

NEQUICK2_DEFAULT_INPUT = OrderedDict([('lat1', -26.9),
                                      ('lon1', 294.6),
                                      ('h1', 80),
                                      ('lat2', None),
                                      ('lon2', None),
                                      ('h2', 1000),
                                      ('year', 2014),
                                      ('month', 1),
                                      ('day', 13),
                                      ('hour', 4),
                                      ('localtime', 0),
                                      ('user', 0),
                                      ('r12_f', 1),
                                      ('sol_val', 0),
                                      ('itu', 1)])
