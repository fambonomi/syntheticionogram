# -*- coding: utf-8 -*-
"""
    Handlers para las versiones web de los modelos IRI y NeQuick2
"""
import os
import urllib
import urllib.parse
import urllib.request
from .inputspresets import IRI2016_DEFAULT_INPUT, NEQUICK2_DEFAULT_INPUT
from .parsers import IRIParser, NeQuick2Parser, NeQuick2ParserException, IRIParserException
import time
IRI_URL = "https://omniweb.gsfc.nasa.gov/cgi/vitmo/vitmo_model.cgi"
NEQUICK2_URL = "https://t-ict4d.ictp.it/nequick2/nequick"

class ModelHandlerException(Exception):
    pass
class ModelHandler(object):
    """Clase base para derivar handlers para modelos webs"""

    def __init__(self, model_inputs, url, parser, **kwargs):
        self.model_inputs = model_inputs
        self.url = url
        self.parser = parser()

        self.result = None
        self.save_folder = "corridas/"
        self.file_name = "{year}-{month}-{day}.{hour}.txt".format(**kwargs)
        for key, value in kwargs.items():
            self.model_inputs[key] = value


    def get_model_output(self):
        """
        Retorna la salida HTML de los modelos
        """
        if os.path.isfile(self.save_folder + self.file_name):
            with open(self.save_folder + self.file_name, "r") as f:
                self.result = f.read()
            print("Corrida local", self.url)
        else:
            print("Corrida remota", self.url)

            try:
                data = bytes(urllib.parse.urlencode(self.model_inputs,
                                                doseq=True).encode())
                handler = urllib.request.urlopen(self.url, data)
                self.result = handler.read().decode('utf-8')
            except Exception as e:
                with open("log.txt", "a") as f:
                    print(datetime.datetime.now(), "Error corrida: ", self.url, filename, e, file=f)
                    print(datetime.datetime.now(), "Error corrida: ", self.url, filename, e, file=f)
                    raise ModelHandlerException()
            with open(self.save_folder + self.file_name, "w") as f:
                    f.write(self.result)

    def get_model_result(self):
        """Retorna un diccionario que contiene las claves:
            metadata: Información extra que devuelve la salida de los modelos
            array: numpy.array con los resultados del modelos
            labels: lista de etiquetas de cada columna del array
            units: unidades de las columnas del array
        """
        self.get_model_output()
#        try:
        self.parser.feed(self.result)
        return self.parser.get_model_result()
#       except Exception as e:
#          print(e)
            #if self.url == NEQUICK2_URL:
            #    raise NeQuick2ParserException()
            #elif self.url == IRI_URL:
            #    raise IRIParserException()




class IRI(ModelHandler):
    """
        Handler para la versión web del modelo IRI
    """

    def __init__(self, **kwargs):
        super(IRI, self).__init__(model_inputs=IRI2016_DEFAULT_INPUT,
                                  url=IRI_URL, parser=IRIParser,
                                  **kwargs)
        time.sleep(1)
        self.save_folder = "corridas/iri2016/"



class NeQuick2(ModelHandler):
    """
    Handler para la versión web del modelo NeQuick2
    """

    def __init__(self, **kwargs):
        super(NeQuick2, self).__init__(model_inputs=NEQUICK2_DEFAULT_INPUT,
                                       url=NEQUICK2_URL, parser=NeQuick2Parser,
                                       **kwargs)
        time.sleep(0.5)
        if self.model_inputs["lat2"] is None:
            self.model_inputs["lat2"] = self.model_inputs["lat1"]

        if self.model_inputs["lon2"] is None:
            self.model_inputs["lon2"] = self.model_inputs["lon1"]
        self.save_folder = "corridas/nequick2/"
