import numpy as np
import pandas as pd

from profiles import IRIfof2HourProfile

results = []
arrays = []
for day in range(1, 31 + 1):
    print("Calculando día %s" % day)
    result = IRIfof2HourProfile(latitude=-26.9, longitude=294.6,
                           year=2014, month=1, day=day,
                           start=0., stop=23.9, step=0.16667)
    results.append(result)
    arrays.append(result.model_result["array"])

a = np.concatenate(arrays)
df = pd.DataFrame(a.astype(float), columns=results[0].model_result["labels"])
df = df.set_index(["1 Year", "2 Month", "3 Day", "4 Hour"])
df.to_csv("201401-m3000f2-fof2.csv")
