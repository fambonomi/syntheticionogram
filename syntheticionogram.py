""" Plot synthetics ionograms"""
import datetime
import os
from matplotlib import pyplot as plt
import matplotlib.ticker as ticker
from ionospherepropagation import compute_ionogram
from profiles import IRINeProfile, NeQuick2NeProfile
from webmodels.handlers import IRIParserException, NeQuick2ParserException
import numpy as np
import time
import gc

STATIONS = {'TUJ2O': {"latitude": -26.9, "longitude": 294.6}}


def transform_to_ionogram_scale(x, y):
    """
    Transformación de los datos para que concuerden con los ejes
    de los ionogramas. Se toman como referencia los 2 puntos extremos:
    (1MHz, 100Km) y (20MHz, 760Km); y la posición en píxeles de éstos:
    (38, 330) y (798, 30) respectivamente.
    b1 + c1 = 38; 100*b2 + c2 = 330; 20*b1 + c1 = 798; 760*b2 + c2 = 30
    """
    return (x / 1e6) * 40 - 2, y * (-5 / 11) + (4130 / 11)


class IonogramFile:
    def __init__(self, path):
        self.filename = filename = os.path.basename(path)
        self.station = filename[0:5]
        self.year = filename[6:10]
        self.doy = filename[10:13]
        self.hour = filename[13:15]
        self.minute = filename[15:17]
        self.date = datetime.datetime.strptime(
            "".join((self.year, self.doy, self.hour, self.minute)), "%Y%j%H%M")
        self.month = str(self.date.month).zfill(2)
        self.day = str(self.date.day).zfill(2)
        self.decimal_hour = str(int(self.hour) + int(self.minute) / 60)


def plot_over_real_ionogram(ionogram_path, save_folder="output"):
    ionogram = IonogramFile(ionogram_path)
    year = ionogram.year
    month = ionogram.month
    day = ionogram.day
    ut = ionogram.decimal_hour

    latitude = STATIONS[ionogram.station]["latitude"]
    longitude = STATIONS[ionogram.station]["longitude"]

    save_name = "-".join([ionogram.station, ionogram.year,
                          ionogram.month, ionogram.day,
                          str(ionogram.hour)+str(ionogram.minute)]) + "UT.png"
    if  os.path.isfile(os.path.join(save_folder, save_name)):
        return None
    iri_profile = IRINeProfile(year=year, month=month,
                               day=day, ut=ut,
                               latitude=latitude,
                               longitude=longitude)

    nequick2_profile = NeQuick2NeProfile(year=year, month=month,
                                         day=day, ut=ut,
                                         latitude=latitude,
                                         longitude=longitude)

    iri_ionogram = compute_ionogram(iri_profile.electron_densities,
                                    iri_profile.heights)

    nequick2_ionogram = compute_ionogram(nequick2_profile.electron_densities,
                                         nequick2_profile.heights)

    reescaled_iri_ionogram = np.array([transform_to_ionogram_scale(x, y)
                                       for x, y in iri_ionogram])
    reescaled_nequick2_ionogram = np.array([transform_to_ionogram_scale(x, y)
                                            for x, y in nequick2_ionogram])


    dpi = 300
    fig, ax = plt.subplots()

    # ax = plt
    fig.patch.set_visible(False)
    fig.set_size_inches(998 / 300, 363 / 300)
    ax.axis('off')

    ionogram_img = plt.imread(ionogram_path)
    ax.imshow(ionogram_img)
    ax.plot(reescaled_iri_ionogram[:, 0],
            reescaled_iri_ionogram[:, 1], c="#1f78b4",label="IRI-CCIR-ABT-AMTB", alpha=0.7)
    ax.plot(reescaled_nequick2_ionogram[:, 0],
            reescaled_nequick2_ionogram[:, 1], c="#e31a1c", label="NeQuick2", alpha=0.7)

    plt.xlim(0, 998)
    plt.ylim(363, 0)
    legend = plt.legend(fontsize=3.5, loc='lower right', frameon=False)
    for text in legend.get_texts():
        plt.setp(text, color='w')

    plt.tight_layout()
    plt.gca().set_axis_off()
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                        hspace=0, wspace=0)
    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())


    os.makedirs(save_folder, exist_ok=True)

    plt.savefig(os.path.join(save_folder, save_name), dpi=dpi, pad_inches=0)
    plt.cla()
    del ionogram
    del iri_profile
    del nequick2_profile
    del iri_ionogram
    del nequick2_ionogram
    del reescaled_iri_ionogram
    del reescaled_nequick2_ionogram
    del ionogram_img
    del ax
    del fig
    gc.collect()


def plot_ionograms_folder(folder):
    ionograms = sorted(os.listdir(folder), reverse=True)
    while len(ionograms) > 0:
        print("Faltan %s ionogramas"%len(ionograms))
        filename = ionograms.pop()
        print(filename)
        try:
            plot_over_real_ionogram(os.path.join(folder, filename))
        except Exception as e:
            with open("log.txt", "a") as f:
                print(datetime.datetime.now(), "Error: ", filename, e, file=f)
                print(datetime.datetime.now(), "Error: ", filename, e)
            ionograms.insert(0, filename)

plot_ionograms_folder("../tucuman/sondador-gif-ene14/")  # Reemplazar con directorio con ionogramas
