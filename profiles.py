# -*- coding: utf-8 -*-
"""Perfiles de densidad electrónica de los modelos IRI y NeQuick2"""
from webmodels.handlers import IRI, NeQuick2


class IRIfof2HourProfile(object):
    """Clase base para perfiles de densidad electrónica"""

    def __init__(self, latitude, longitude, year, month, day, start, stop, step,
                 model_input=None):
        if model_input is None:
            self.model_input = {}
        self.model_input["year"] = year
        self.model_input["month"] = month
        self.model_input["day"] = day
        self.model_input["profile"] = 8  # hour profile
        self.model_input["start"] = start
        self.model_input["stop"] = stop
        self.model_input["step"] = step
        self.model_input["vars"] = ['00', '01', '02', '04', '39', '44']

        model_result = IRI(**self.model_input).get_model_result()
        self.model_result = model_result
        self.year = model_result["array"][:, 0].astype(float)
        self.month = model_result["array"][:, 1].astype(float)
        self.day = model_result["array"][:, 2].astype(float)
        self.hour = model_result["array"][:, 3].astype(float)
        self.m300f2 = model_result["array"][:, 4].astype(float)
        self.fof2 = model_result["array"][:, 5].astype(float)


class NeProfile(object):
    """Clase base para perfiles de densidad electrónica"""

    def __init__(self, latitude, longitude, year, month, day, ut,
                 model_input=None):
        if model_input is None:
            self.model_input = {}
        self.model_input["year"] = year
        self.model_input["month"] = month
        self.model_input["day"] = day
        self.model_input["hour"] = ut
        self._electron_densities = None
        self._heights = None
        self._latitude = latitude
        self._longitude = longitude

    @property
    def electron_densities(self):
        """ Electron densities list """
        return self._electron_densities

    @property
    def heights(self):
        """ heights list in Km """
        return self._heights


class IRINeProfile(NeProfile):
    """IRI electron density profile."""

    def __init__(self, latitude, longitude, year, month, day, ut,
                 model_input=None):
        NeProfile.__init__(self, latitude, longitude, year, month, day, ut,
                           model_input)
        self.model_input["latitude"] = latitude
        self.model_input["longitude"] = longitude
        self.model_input["vars"] = ['6', '17']
        model_result = IRI(**self.model_input).get_model_result()
        self._electron_densities = model_result["array"][:, 1].astype(float)
        self._heights = model_result["array"][:, 0].astype(float)


class NeQuick2NeProfile(NeProfile):
    """NeQuick2 electron density profile."""

    def __init__(self, latitude, longitude, year, month, day, ut,
                 model_input=None):
        NeProfile.__init__(self, latitude, longitude, year, month, day, ut,
                           model_input)
        if model_input is None:
            model_input = {}
        self.model_input["lat1"] = latitude
        self.model_input["lon1"] = longitude
        model_result = NeQuick2(**self.model_input).get_model_result()
        self._electron_densities = model_result["array"][:, 4].astype(float)
        self._heights = model_result["array"][:, 1].astype(float)
