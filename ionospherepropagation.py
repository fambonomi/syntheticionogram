# -*- coding: utf-8 -*-
"""
    Permite calcular los puntos de un ionograma a partir de un perfil de
    densidad electrónica
"""
from math import sqrt
from scipy.constants import electron_mass, elementary_charge, epsilon_0, pi
import numpy as np
import better_exceptions  # Opcional, para desarrollo


k = (elementary_charge ** 2) / ((epsilon_0 * electron_mass) * (2 * pi) ** 2)


def compute_plasma_frequency(electron_density):
    """ Plasma frequency"""
    return sqrt(k * electron_density)


def compute_ionogram_point(electron_densities, heights, index):
    """Determina la altura virtual en un sondaje de base y frecuencia de plasma
    para el punto correspondiente al indice dado en un perfil de densidad
    electrónica dado por los vectores ne_profile y altitudes (densidades
    y alturas reales, respectivamente).
    Si el punto de reflexión corresponde a un índice inferior retorna [0,0]
    """
    plasma_frequency = compute_plasma_frequency(electron_densities[index])

    def integrando(electron_density):
        return sqrt(1 - k * electron_density / plasma_frequency ** 2)

    dh = heights[1] - heights[0]

    try:
        virtual_height = (sum([integrando(Ne)
                               for Ne in electron_densities[0:index]]) * dh
                          + heights[0])

        return (plasma_frequency, virtual_height)
    except ValueError:
        """Esto ocurrirá si el argumento de la raiz se vuelve negativo, lo que
        significa que la reflexión ocurrió en un punto correspondiente a un
        índice más bajo que el dado"""
        return (0, 0)


def compute_ionogram(electron_densities, heights):
    # Calcular los puntos del ionograma para cada punto del del bottomside
    ionogram = [compute_ionogram_point(electron_densities, heights, i)
                for i in range(np.argmax(electron_densities))]

    # Filtrar valores menores a un MHz
    return np.array([x for x in ionogram
                     if x[0] >= 1e6])


if __name__ == '__main__':
    """ Ejemplo de datos para 2014-01-13 04:00 UT - Tucumán
     Perfil de Ne de 70 Km a 1000 Km, pasos de a 5Km"""
    from matplotlib import pyplot as plt
    from profiles import IRINeProfile, NeQuick2NeProfile

    year = 2014
    month = 1
    day = 13
    ut = 4.
    tucuman_latitude = -26.9
    tucuman_longitude = 294.6

    iri_profile = IRINeProfile(year=year, month=month,
                               day=day, ut=ut,
                               latitude=tucuman_latitude,
                               longitude=tucuman_longitude)

    nequick2_profile = NeQuick2NeProfile(year=year, month=month,
                                         day=day, ut=ut,
                                         latitude=tucuman_latitude,
                                         longitude=tucuman_longitude)

    ionograma_iri = compute_ionogram(iri_profile.electron_densities,
                                     iri_profile.heights)

    ionograma_nequick2 = compute_ionogram(nequick2_profile.electron_densities,
                                          nequick2_profile.heights)

    plt.plot(ionograma_iri[:, 0], ionograma_iri[:, 1])
    plt.plot(ionograma_nequick2[:, 0], ionograma_nequick2[:, 1], c="red")
    plt.xlim(1e6, 15e6)
    plt.xticks(np.arange(1e6, 16e6, 1e6), range(1, 16))
    plt.xlabel("f (MHz)")
    plt.ylim(100, 760)
    plt.ylabel("h' (Km)")
    plt.savefig('ionograma_sintetico.pdf')
